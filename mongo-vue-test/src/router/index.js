import Vue from 'vue'
import VueRouter from 'vue-router'
import Experiment5 from "@/views/Experiment5"

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Experiment5',
    component: Experiment5
  },
]

const router = new VueRouter({
  routes
})

export default router
