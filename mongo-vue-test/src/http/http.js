import axios from 'axios'
import qs from 'qs'

axios.defaults.baseURL = '/api'

const $http = {
    post: "",
    get: "",
}

$http.post = (url, data) => {
    if (!(data instanceof FormData)) {
        data = qs.stringify(data)
    }
    return new Promise(resolve => {
        axios.post(url, data).then(res => {
            resolve(res.data)
        }).catch(err => {
            resolve(err.response)
        })
    })
}

$http.get = (url, data) => {
    let query = qs.stringify(data)
    if (query) {
        query = "?" + query
    }
    return new Promise(resolve => {
        axios.get(url + query).then(res => {
            resolve(res.data)
        }).catch(err => {
            resolve(err.response)
        })
    })
}

export default $http