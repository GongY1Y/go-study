import $http from '@/http/http'

export const getMyCourse = (sid => {
    return $http.get('/getMyCourse?sid=' + sid)
})