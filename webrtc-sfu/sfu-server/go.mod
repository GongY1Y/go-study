module sfu-server

go 1.16

require (
	github.com/chuckpreslar/emission v0.0.0-20170206194824-a7ddd980baf9
	github.com/gorilla/websocket v1.5.0
	github.com/pion/rtcp v1.2.3
	github.com/pion/rtp v1.6.0
	github.com/pion/webrtc/v2 v2.2.26
	github.com/rs/zerolog v1.26.1
	github.com/stretchr/testify v1.7.0 // indirect
	gopkg.in/ini.v1 v1.66.4
)
