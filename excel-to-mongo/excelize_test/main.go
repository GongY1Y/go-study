package main

import (
	"fmt"
	"github.com/360EntSecGroup-Skylar/excelize"
)

func main() {
	studentData, err := excelize.OpenFile("./student.xlsx")
	if err != nil {
		fmt.Println("student.xlsx 导入失败，请检查文件目录是否正确")
		return
	}

	rows := studentData.GetRows("Sheet1")
	for _, row := range rows {
		for _, value := range row {
			fmt.Printf("\t%s", value)
		}
		fmt.Println()
	}
}
