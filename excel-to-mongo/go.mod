module excel-to-mongo

go 1.16

require (
	github.com/360EntSecGroup-Skylar/excelize v1.4.1
	go.mongodb.org/mongo-driver v1.7.3
)
