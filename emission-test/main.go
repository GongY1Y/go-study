package main

import (
	"fmt"
)

import (
	"github.com/chuckpreslar/emission"
)

func main() {
	emitter := emission.NewEmitter()

	emitter.On("test", test)
	emitter.Emit("test", "hello world")
}

func test(str string) {
	fmt.Println(str)
}
