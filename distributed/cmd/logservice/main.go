package main

import (
	"context"
	"distributed/log"
	"distributed/registry"
	"distributed/service"
	"fmt"
	stdLog "log"
)

func main() {
	// 自定义的 log 进行初始化，并指定写入的文件
	log.Run("./distributed.log")

	// 指定主机和端口
	// 这些信息通常是由配置文件或环境变量读取
	host, port := "localhost", "4000"

	serviceAddress := fmt.Sprintf("http://%s:%s", host, port)

	r := registry.Registration{
		ServiceName:      registry.LogService,
		ServiceURL:       serviceAddress,
		RequiredServices: make([]registry.ServiceName, 0),
		ServiceUpdateURL: serviceAddress + "/services",
		HeartbeatURL:     serviceAddress + "/heartbeat",
	}

	// 启动服务
	ctx, err := service.Start(
		context.Background(),
		host,
		port,
		r,
		log.RegisterHandlers, // 注册函数
	)
	if err != nil {
		// 如果错误不为空，我们将使用标准库中的 logger
		// 因为我们的自定义 logger 没有启动成功
		stdLog.Fatalln(err)
	}

	// 等 context 的信号，启动 http 服务器时出现错误或手动停止时
	<-ctx.Done()

	fmt.Println("Shutting down log service.")
}
