package main

import (
	"context"
	"distributed/registry"
	"fmt"
	"log"
	"net/http"
)

func main() {
	// 启动健康检查服务
	registry.SetupRegistryService()

	// registry.RegistryService 实现了 Handle 接口，可提供 HTTP 服务
	http.Handle("/services", &registry.RegistryService{})

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	var srv http.Server
	srv.Addr = registry.ServerPort

	// 发生错误时，服务终止
	go func() {
		log.Println(srv.ListenAndServe())
		cancel()
	}()

	// 手动终止服务
	go func() {
		fmt.Println("Registry service started. Press any key to stop.")
		var s string
		fmt.Scanln(&s)
		srv.Shutdown(ctx)
		cancel()
	}()

	<-ctx.Done()
	fmt.Println("Shutting down registry service")
}
