package registry

// Registration 服务注册信息
type Registration struct {
	ServiceName      ServiceName
	ServiceURL       string
	RequiredServices []ServiceName // 服务所依赖的其他服务
	ServiceUpdateURL string        // 便于接收服务注册中心的消息
	HeartbeatURL     string        // 用作心跳检查
}

// ServiceName 自定义类型
type ServiceName string

// 把存在的服务行程一个常量
const (
	LogService     = ServiceName("LogService")
	GradingService = ServiceName("GradingService")
	PortalService  = ServiceName("Portal")
)

// 每个条目中的内容
type patchEntry struct {
	Name ServiceName
	URL  string
}

// 增加和减少的条目
type patch struct {
	Added   []patchEntry
	Removed []patchEntry
}
