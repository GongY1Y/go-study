package log

import (
	"bytes"
	"distributed/registry"
	"fmt"
	"net/http"

	stdLog "log"
)

// 为了让客户端的服务方便地使用 log service

func SetClientLogger(serviceURL string, clientService registry.ServiceName) {
	stdLog.SetPrefix(fmt.Sprintf("[%v] - ", clientService))
	stdLog.SetFlags(0)
	stdLog.SetOutput(&clientLogger{url: serviceURL})
}

type clientLogger struct {
	url string
}

func (cl clientLogger) Write(data []byte) (int, error) {
	// 发送 POST 请求，将数据发送到 log 服务器
	b := bytes.NewBuffer(data)
	res, err := http.Post(cl.url+"/log", "text/plain", b)
	if err != nil {
		return 0, err
	}
	if res.StatusCode != http.StatusOK {
		return 0, fmt.Errorf("failed to send log message. Service responded with %d - %s", res.StatusCode, res.Status)
	}
	return len(data), nil
}
