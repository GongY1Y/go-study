package main

import (
	"context"
	"fmt"
	"google.golang.org/grpc"
	"grpc-test/pb/person"
	"sync"
	"time"
)

func main() {
	l, _ := grpc.Dial("127.0.0.1:8080", grpc.WithInsecure())
	client := person.NewSearchServiceClient(l)

	//一元 RPC
	//res, err := client.Search(context.Background(), &person.PersonReq{Name: "Hello"})
	//if err != nil {
	//	panic(err.Error())
	//}
	//fmt.Println(res)

	// 客户端流式 RPC
	//c, err := client.SearchIn(context.Background())
	//if err != nil {
	//	panic(err.Error())
	//}
	//for i := 0; i < 10; i++ {
	//	time.Sleep(1 * time.Second)
	//	err = c.Send(&person.PersonReq{Name: "information"})
	//	if err != nil {
	//		panic(err.Error())
	//	}
	//}
	//res, err := c.CloseAndRecv()
	//if err != nil {
	//	panic(err.Error())
	//}
	//fmt.Println(res)

	// 服务端流式 RPC
	//c, err := client.SearchOut(context.Background(), &person.PersonReq{Name: "hello"})
	//if err != nil {
	//	panic(err.Error())
	//}
	//
	//for {
	//	res, err := c.Recv()
	//	if err != nil {
	//		fmt.Println(err)
	//		break
	//	}
	//	fmt.Println(res)
	//}

	// 双向流式 RPC
	c, err := client.SearchIO(context.Background())
	if err != nil {
		panic(err.Error())
	}
	wg := sync.WaitGroup{}
	wg.Add(2)
	go func() {
		for {
			time.Sleep(time.Second)
			err = c.Send(&person.PersonReq{Name: "Hello"})
			if err != nil {
				fmt.Println(err)
				wg.Done()
				break
			}
		}
	}()
	go func() {
		for {
			res, err := c.Recv()
			if err != nil {
				fmt.Println(err)
				wg.Done()
				break
			}
			fmt.Println(res)
		}
	}()
	wg.Wait()
}
