package main

import (
	"fmt"
	"go-study/algorithm/common"
)

// 如何在只给定单链表中中某个节点指针的情况下删除该节点

func main() {
	head := &common.LNode{}
	common.CreateNode1(head, 8)

	deleteNode := head.Next.Next.Next.Next.Next
	fmt.Println("删除节点前：", head)
	removeNode(deleteNode)
	fmt.Println("删除节点后：", head)
}

func removeNode(node *common.LNode) bool {
	if node == nil || node.Next == nil {
		return false
	}

	node.Data = node.Next.Data
	node.Next = node.Next.Next

	return true
}
