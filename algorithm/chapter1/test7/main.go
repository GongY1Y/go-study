package main

import (
	"fmt"
	"go-study/algorithm/common"
)

// 如何把相邻元素翻转
// 翻转前 1 -> 2 -> 3 -> 4 -> 5
// 翻转后 2 -> 1 -> 3 -> 4 -> 5

func main() {
	head := &common.LNode{}
	common.CreateNode1(head, 8)
	fmt.Println("顺序输出：", head)
	reverse(head)
	fmt.Println("逆序输出：", head)
}

func reverse(head *common.LNode) {
	if head == nil || head.Next == nil {
		return
	}

	previous := head
	current := head.Next
	var next *common.LNode

	for current != nil && current.Next != nil {
		next = current.Next

		current.Next = next.Next
		previous.Next = next
		next.Next = current

		previous = current
		current = current.Next
	}
}
