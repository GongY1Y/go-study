package main

import "fmt"

// 如何判断一个字符串是否包含重复字符

func main() {
	str := "GOOD"
	fmt.Println(isDup(str))
}

func isDup(str string) bool {
	flag := make([]int, 8) // 只需要 8 个 32 位的 int， 8 * 32 = 256
	for _, v := range str {
		index := int(v) / 32
		shift := uint(v) % 32
		if flag[index]&(1<<shift) != 0 {
			return true
		}
		flag[index] |= 1 << shift
	}
	return false
}
