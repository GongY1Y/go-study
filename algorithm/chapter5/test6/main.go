package main

import "fmt"

// 如何对由大小写字母组成的字符数组排序
// 只需要小写字母在大写字母前面

func main() {
	ch := []rune("AbcDef")
	reverseArray(ch)
	fmt.Println(string(ch))
}

func reverseArray(ch []rune) {
	begin := 0
	end := len(ch) - 1
	for begin < end {
		for ch[begin] >= 'a' && ch[begin] <= 'z' && begin < end {
			begin++
		}

		for ch[end] >= 'A' && ch[end] <= 'Z' && begin < end {
			end--
		}

		ch[begin], ch[end] = ch[end], ch[begin]
	}
}
