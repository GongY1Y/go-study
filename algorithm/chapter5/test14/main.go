package main

import "fmt"

// 如何统计字符串中连续重复字符的个数（用递归实现）

func main() {
	fmt.Println(getMaxDupChar("abbc", 0, 0, 1))
	fmt.Println(getMaxDupChar("aaabbcc", 0, 0, 1))
}

func getMaxDupChar(s string, startIndex, curMaxLen, maxLen int) int {
	// 字符串遍历结束，返回最长连续重复字符串的长度
	if startIndex == len(s)-1 {
		return max(curMaxLen, maxLen)
	}

	// 如果两个连续的字符相等，则在递归调用的时候把当前最大串的长度加 1
	if s[startIndex] == s[startIndex+1] {
		return getMaxDupChar(s, startIndex+1, curMaxLen+1, maxLen)
	} else {
		// 两个连续的子符不相等
		// 当前连续重复字符串的长度变为 1
		// 求出最长串
		return getMaxDupChar(s, startIndex+1, 1, max(curMaxLen, maxLen))
	}
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}
