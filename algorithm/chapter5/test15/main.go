package main

import "fmt"

// 如何求最长递增子序列的长度

func main() {
	fmt.Println(getMaxAscendingLen("xbcdza"))
}

func getMaxAscendingLen(str string) int {
	maxLen := make([]int, len(str))
	maxAscendingLen := 1
	for i := range str {
		maxLen[i] = 1
		for j := 0; j < i; j++ {
			if str[j] < str[i] && maxLen[j]+1 > maxLen[i] {
				maxLen[i] = maxLen[j] + 1
				maxAscendingLen = maxLen[i]
			}
		}
	}

	return maxAscendingLen
}
