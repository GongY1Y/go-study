package main

import "fmt"

// 如何判断字符串是整数

func main() {
	printResult("-543")
	printResult("6543")
	printResult("+543")
	printResult("++43")
	printResult2("-543")
	printResult2("6543")
	printResult2("+543")
	printResult2("++43")
}

// 非递归法
func strToInt2(str string) int {
	arr := []rune(str)
	flag = true
	res := 0

	i := 0
	minus := false
	if arr[i] == '-' {
		minus = true
		i++
	}
	if arr[i] == '+' {
		i++
	}

	for ; i < len(arr); i++ {
		if isNumber(arr[i]) {
			res = res*10 + int(arr[i]-'0')
		} else {
			flag = false
			fmt.Println("不是数字")
			return -1
		}
	}

	if minus {
		return -res
	} else {
		return res
	}
}

func printResult2(s string) {
	re := strToInt2(s)
	if flag {
		fmt.Println(re)
	}
}

// 递归法
func strToIntSub(arr []rune, length int) int {
	if length > 1 {
		// 判断尾数
		if !isNumber(arr[length-1]) {
			fmt.Println("不是数字")
			flag = false
			return -1
		}

		// 根据正负加或减尾数
		if arr[0] == '-' {
			return strToIntSub(arr, length-1)*10 - int(arr[length-1]-'0')
		} else {
			return strToIntSub(arr, length-1)*10 + int(arr[length-1]-'0')
		}
	} else {
		if arr[0] == '-' {
			return 0
		} else {
			if !isNumber(arr[0]) {
				fmt.Println("不是数字")
				flag = false
				return -1
			}
			return int(arr[0] - '0')
		}
	}
}

var flag bool

func strToInt(s string) int {
	arr := []rune(s)
	flag = true
	if arr[0] == '+' {
		return strToIntSub(arr[1:], len(arr)-1)
	} else {
		return strToIntSub(arr, len(arr))
	}
}

func printResult(s string) {
	result := strToInt(s)
	if flag {
		fmt.Println(result)
	}
}

func isNumber(a rune) bool {
	return a >= '0' && a <= '9'
}
