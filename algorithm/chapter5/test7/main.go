package main

import (
	"bytes"
	"fmt"
)

// 如何消除字符串的内嵌括号

func main() {
	str := "(1,(2,3),(4,(5,6),7))"
	fmt.Println(removeNestedPare(str))
}

func removeNestedPare(str string) string {
	arr := []rune(str)
	parenthesesNum := 0

	if arr[0] != '(' || arr[len(arr)-1] != ')' {
		return ""
	}

	var buf bytes.Buffer
	buf.WriteRune('(')

	for _, v := range arr {
		switch v {
		case '(':
			parenthesesNum++
		case ')':
			parenthesesNum--
		default:
			buf.WriteRune(v)
		}
		if parenthesesNum < 0 {
			fmt.Println("括号不匹配")
			return ""
		}
	}

	buf.WriteRune(')')

	return buf.String()
}
