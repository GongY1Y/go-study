package main

import (
	"fmt"
	"sort"
)

// 求一个串中出现的第一个最长重复子串

func main() {
	txt := "banana"
	fmt.Println(getMaxCommonStr(txt))
}

// 获取最长的公共子串
func getMaxCommonStr(txt string) string {
	n := len(txt)
	suffixes := make([]string, n) // 用来存储后缀数组
	longestSubStrLen := 0
	var longestSubStr string

	// 获取到后缀数组
	for i := 0; i < n; i++ {
		suffixes[i] = txt[i:]
	}
	sort.Strings(suffixes)

	for i := 1; i < n; i++ {
		tmp := maxPrefix(suffixes[i], suffixes[i-1])
		if tmp > longestSubStrLen {
			longestSubStrLen = tmp
			longestSubStr = (suffixes[i])[0:tmp]
		}
	}

	return longestSubStr
}

// 获取最长的公共子串长度
func maxPrefix(s1, s2 string) int {
	i := 0
	for i < len(s1) && i < len(s2) {
		if s1[i] == s2[i] {
			i++
		} else {
			break
		}
	}

	return i
}
