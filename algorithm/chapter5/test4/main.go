package main

import "fmt"

// 如何判断两个字符串是否为换位字符串

func main() {
	str1 := "aaaabbc"
	str2 := "abcbaab"
	fmt.Println(compare(str1, str2))
}

func compare(s1, s2 string) bool {
	bCount := make([]int, 256)

	for _, v := range s1 {
		bCount[v]++
	}

	for _, v := range s2 {
		bCount[v]--
	}

	for _, v := range bCount {
		if v != 0 {
			return false
		}
	}

	return true
}
