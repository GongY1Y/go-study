package common

import "strconv"

type LNode struct {
	Data int
	Next *LNode
}

func (node *LNode) String() string {
	var str string
	current := node
	if current != nil {
		current = current.Next
	}
	for current != nil {
		str += strconv.Itoa(current.Data) + " "
		current = current.Next
	}
	return str
}

func CreateNode1(head *LNode, length int) {
	current := head
	for i := 1; i < length; i++ {
		current.Next = &LNode{
			Data: i,
		}
		current = current.Next
	}
}

func CreateNode2(head *LNode) {
	data := []int{1, 3, 1, 5, 5, 7}
	current := head
	for i := 0; i < len(data); i++ {
		current.Next = &LNode{
			Data: data[i],
		}
		current = current.Next
	}
}

func CreateNode3(head1, head2 *LNode) {
	// 3 4 5 6 7 8
	current := head1
	for i := 0; i < 6; i++ {
		current.Next = &LNode{
			Data: i + 3,
		}
		current = current.Next
	}

	// 9 8 7 6 5
	current = head2
	for i := 0; i < 5; i++ {
		current.Next = &LNode{
			Data: 9 - i,
		}
		current = current.Next
	}
}

func CreateNode4(head *LNode, length int) {
	current := head
	for i := 1; i < length; i++ {
		current.Next = &LNode{
			Data: i,
		}
		current = current.Next
	}
	current.Next = head.Next.Next.Next
}

func CreateNode(head *LNode, data []int) {
	current := head
	for i := 0; i < len(data); i++ {
		current.Next = &LNode{
			Data: data[i],
		}
		current = current.Next
	}
}
