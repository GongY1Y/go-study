package common

import "errors"

type LinkedQueue struct {
	Head *LNode
	End  *LNode
}

// IsEmpty 检查队列是否为空
func (p *LinkedQueue) IsEmpty() bool {
	return p.Head == nil
}

// Size 查看队列中元素个数
func (p *LinkedQueue) Size() int {
	size := 0
	current := p.Head
	for current != nil {
		size++
		current = current.Next
	}
	return size
}

// Push 入队列
func (p *LinkedQueue) Push(e int) {
	node := &LNode{Data: e}
	if p.Head == nil {
		p.Head = node
		p.End = node
	} else {
		p.End.Next = node
		p.End = node
	}
}

// PushTop 将元素添加到队列头
func (p *LinkedQueue) PushTop(e int) {
	p.Head = &LNode{
		Data: e,
		Next: p.Head,
	}
	if p.End == nil {
		p.End = p.Head
	}
}

// Pop 出队列
func (p *LinkedQueue) Pop() int {
	if p.Head == nil {
		panic(errors.New("队列已经为空"))
	}
	e := p.Head.Data
	p.Head = p.Head.Next
	if p.Head == nil {
		p.End = nil
	}
	return e
}

// GetFront 获取队列首元素
func (p *LinkedQueue) GetFront() int {
	if p.Head == nil {
		panic(errors.New("队列已经为空"))
	}
	return p.Head.Data
}

// GetBack 获取队列尾元素
func (p *LinkedQueue) GetBack() int {
	if p.End == nil {
		panic(errors.New("队列已经为空"))
	}
	return p.End.Data
}
