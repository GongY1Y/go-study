package main

import (
	"fmt"
	"math"
	"math/rand"
	"time"
)

// 如何根据已知随机数生成函数计算新的随机数

func main() {
	for i := 0; i < 16; i++ {
		fmt.Print(rand10(), " ")
	}
	fmt.Println()
}

func rand10() int {
	x := math.MaxInt32
	for x > 40 {
		x = (rand7())*7 + rand7()
	}

	return x%10 + 1
}

func rand7() int {
	rand.Seed(time.Now().UnixNano())
	return rand.Intn(7)
}
