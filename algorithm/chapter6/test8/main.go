package main

import "fmt"

// 如何求有序数列的第 1500 个数的值

func main() {
	fmt.Println(search1(1500))
	fmt.Println(search2(1500))
}

func search1(n int) int {
	i, count := 0, 0
	for true {
		i++
		if i%2 == 0 || i%3 == 0 || i%5 == 0 {
			count++
		}
		if count == n {
			break
		}
	}
	return i
}

func search2(n int) int {
	a := []int{0, 2, 3, 4, 5, 6, 8, 9, 10, 12, 14, 15, 16, 18, 20, 21, 22, 24, 25, 26, 27, 28, 30}
	return (n/22)*30 + a[n%22]
}
