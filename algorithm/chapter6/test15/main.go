package main

import "fmt"

// 如何不使用循环输出 1 到 100

func main() {
	print1(100)
}

func print1(n int) {
	if n > 0 {
		print1(n - 1)
		fmt.Print(n, " ")
	}
}
