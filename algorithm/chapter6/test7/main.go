package main

import "fmt"

// 如何按要求比较两个数的大小

func main() {
	fmt.Println(max(5, 6))
}

func max(a, b int) int {
	if (a-b)&(1<<31) != 0 {
		return b
	} else {
		return a
	}
}
