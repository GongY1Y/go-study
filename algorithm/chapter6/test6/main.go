package main

import "fmt"

// 如何判断 1024! 末尾有多少个 0

func main() {
	fmt.Println(zeroCount(1024))
}

func zeroCount(n int) int {
	count := 0
	for n > 0 {
		n = n / 5
		count += n
	}

	return count
}
