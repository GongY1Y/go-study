package main

import (
	"fmt"
	"strconv"
)

// 如何不使用 ^ 操作实现异或运算

func main() {
	fmt.Println(xor1(3, 5))
	fmt.Println(xor2(3, 5))
}

func xor1(x, y int) int {
	intSize := strconv.IntSize
	res := 0
	var xorBit int
	for i := intSize - 1; i >= 0; i-- {
		b1 := (x & (1 << uint(i))) > 0
		b2 := (y & (1 << uint(i))) > 0

		if b1 == b2 {
			xorBit = 0
		} else {
			xorBit = 1
		}

		res <<= 1
		res |= xorBit
	}

	return res
}

func xor2(x, y int) int {
	return (x | y) & (^x | ^y)
}
