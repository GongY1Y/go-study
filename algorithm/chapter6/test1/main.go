package main

import "fmt"

// 如何判断一个自然是是否是某个数的平方

func main() {
	fmt.Println(isPower(15))
	fmt.Println(isPower(16))
	fmt.Println(isPower2(15))
	fmt.Println(isPower2(16))
}

// 二分查找法
func isPower(n int) bool {
	if n <= 0 {
		return false
	}

	low := 0
	high := n

	for low <= high {
		mid := (low + high) / 2
		power := mid * mid
		if power > n {
			high = mid - 1
		} else if power < n {
			low = mid + 1
		} else {
			return true
		}
	}

	return false
}

// 减法运算法
func isPower2(n int) bool {
	if n <= 0 {
		return false
	}

	for minus := 1; n > 0; minus += 2 {
		n -= minus
		if n == 0 {
			return true
		} else if n < 0 {
			return false
		}
	}

	return false
}
