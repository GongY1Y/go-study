package main

import (
	"fmt"
	"strconv"
)

// 如何找最小的不重复数

func main() {
	fmt.Println(findMinNonDupNum(23345))
	fmt.Println(findMinNonDupNum(1101010))
	fmt.Println(findMinNonDupNum(99010))
	fmt.Println(findMinNonDupNum(8989))
}

func findMinNonDupNum(n int) int {
	count := 0 // 用来记录循环的次数
	mRune := []rune(strconv.Itoa(n + 1))
	ch := make([]rune, len(mRune)+1)

	ch[0] = '0'
	ch[len(ch)-1] = '0'

	for i := 0; i < len(mRune); i++ {
		ch[i+1] = mRune[i]
	}

	ll := len(ch)
	i := 2 // 从左向右遍历
	for i < ll {
		count++
		if ch[i-1] == ch[i] {
			ch[i]++
			carry(ch, i)
			for j := i + 1; j < ll; j++ { // 把下标 i 后面的字符串变为 0101... 串
				if (j-i)%2 == 1 {
					ch[j] = '0'
				} else {
					ch[j] = '1'
				}
			}
		} else {
			i++
		}
	}

	fmt.Println("循环次数：", count)

	result, _ := strconv.Atoi(string(ch))
	return result
}

// 处理数字相加的进位
func carry(num []rune, pos int) {
	for ; pos > 0; pos-- {
		if num[pos] > '9' {
			num[pos] = '0'
			num[pos-1]++
		}
	}
}
