package main

import "fmt"

// 如何只使用 ++ 操作符实现加减乘除运算

func main() {
	fmt.Println(add(2, -4))
	fmt.Println(minus(2, -4))
	fmt.Println(multi(2, 4))
	fmt.Println(divide(9, 4))
}

// 用 ++ 实现加法操作，限制条件：至少有一个非负数
func add(a, b int) int {
	if a < 0 && b < 0 {
		return -1
	}

	if b >= 0 {
		for i := 0; i < b; i++ {
			a++
		}
		return a
	} else {
		for i := 0; i < a; i++ {
			b++
		}
		return b
	}
}

// 用 ++ 实现加减法操作，限制条件：被减数大于减数
func minus(a, b int) int {
	if a < b {
		return -1
	}

	result := 0
	for ; b != a; b++ {
		result++
	}

	return result
}

func multi(a, b int) int {
	if a <= 0 || b <= 0 {
		return -1
	}
	result := 0
	for i := 0; i < b; i++ {
		result = add(result, a)
	}

	return result
}

func divide(a, b int) int {
	if a <= 0 || b <= 0 {
		return -1
	}

	result := 1
	tmpMulti := 0

	for true {
		tmpMulti = multi(b, result)
		if tmpMulti <= a {
			result++
		} else {
			break
		}
	}

	return result - 1
}
