package main

import (
	"bytes"
	"fmt"
)

// 如何把十进制数（long 型）分别以二进制和十六进制形式输出

func main() {
	fmt.Println(intToBinary(10))
	fmt.Println(intToHex(10))
}

func intToBinary(n int) string {
	if n < 0 {
		return ""
	}

	hexNum := 8 * 8 // 二进制的位数（long 占 8 个字节）
	var pBinary bytes.Buffer

	for i := 0; i < hexNum; i++ {
		// 先左移 i 位把 pBinary[i] 移到最高位，然后右移 hexNum-1 位把 pBinary[i] 移到最后一位
		tmpBinary := (n << uint(i)) >> (uint(hexNum) - 1)
		if tmpBinary == 0 {
			pBinary.WriteRune('0')
		} else {
			pBinary.WriteRune('1')
		}
	}

	return pBinary.String()
}

func intToHex(s int) string {
	hex := ""
	remainder := 0
	for s != 0 {
		remainder = s % 16
		if remainder < 10 {
			hex = string(rune(remainder+'0')) + hex
		} else {
			hex = string(rune(remainder-10+'A')) + hex
		}
		s = s / 16
	}

	return hex
}
