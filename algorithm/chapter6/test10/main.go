package main

import "fmt"

// 如何求二进制中 1 的个数

func main() {
	fmt.Println(countOne1(7))
	fmt.Println(countOne1(8))
	fmt.Println(countOne2(7))
	fmt.Println(countOne2(8))
}

func countOne1(n int) int {
	count := 0
	for n > 0 {
		if n&1 == 1 {
			count++
		}
		n >>= 1
	}

	return count
}

func countOne2(n int) int {
	count := 0
	for n > 0 {
		if n != 0 {
			n = n & (n - 1)
		}
		count++
	}
	return count
}
