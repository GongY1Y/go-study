package main

import "fmt"

// 如何判断一个数是否是 2 的 n 次方

func main() {
	fmt.Println(isPower(16))
	fmt.Println(isPower(15))
	fmt.Println(isPower(14))
}

func isPower(n int) bool {
	if n < 1 {
		return false
	}

	return n&(n-1) == 0
}
