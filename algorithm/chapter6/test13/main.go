package main

import "fmt"

// 如何在不能使用库函数的条件下计算 n 的算术平方根

func main() {
	n := 50.0
	e := 0.0000001
	fmt.Println(squareRoot(n, e))
	n = 4.0
	fmt.Println(squareRoot(n, e))
}

// 获取 n 的平方根 e 为精度要求
func squareRoot(n, e float64) float64 {
	newOne := n
	lastOne := 1.0
	for newOne-lastOne > e {
		newOne = (newOne + lastOne) / 2
		lastOne = n / newOne
	}
	return newOne
}
