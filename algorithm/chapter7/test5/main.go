package main

import (
	"fmt"
	"math/rand"
	"time"
)

// 如何等概率地从大小为 n 的数组中选取 m 个整数

func main() {
	rand.Seed(time.Now().UnixNano())
	a := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	n, m := 10, 6
	getRandomM(a, n, m)
	for i := 0; i < m; i++ {
		fmt.Print(a[i], " ")
	}
	fmt.Println()
}

func getRandomM(a []int, n, m int) {
	if a == nil || n <= 0 || n < m {
		return
	}

	for i := 0; i < m; i++ {
		j := i + rand.Intn(n-i)
		a[i], a[j] = a[j], a[i]
	}
}
