package main

import (
	"fmt"
	"math/rand"
	"time"
)

// 如何用一个随机函数得到另外一个随机函数

func main() {
	rand.Seed(time.Now().UnixNano())
	for i := 0; i < 16; i++ {
		fmt.Print(func2(), " ")
	}
	fmt.Println()
}

func func1() int {
	return rand.Intn(2)
}

func func2() int {
	a1 := func1()
	a2 := func1()
	tmp := a1 | (a2 << 1)
	if tmp == 0 {
		return 0
	} else {
		return 1
	}
}
