package main

import (
	"fmt"
	"math/rand"
	"time"
)

// 如何拿到最多金币

func main() {
	rand.Seed(time.Now().UnixNano())
	monitorCount := 1000
	success := 0.0
	for i := 0; i < monitorCount; i++ {
		if getMaxNum(10) {
			success++
		}
	}
	fmt.Println(success / float64(monitorCount))
}

func getMaxNum(n int) bool {
	if n < 10 {
		return false
	}

	a := make([]int, n)

	// 设置随机数
	for i := 0; i < n; i++ {
		a[i] = rand.Intn(1000) + 1
	}

	max4 := 0
	for i := 0; i < 4; i++ {
		if a[i] > max4 {
			max4 = a[i]
		}
	}

	var i int
	for i = 4; i < n; i++ {
		if a[i] > max4 {
			break
		}
	}

	if i == 10 {
		return false
	}

	for j := i + 1; j < n; j++ {
		if a[i] < a[j] {
			return false
		}
	}

	return true
}
