package main

import "fmt"

// 如何组合 1, 2, 5 这三个数使其和为 100

func main() {
	fmt.Println(combinationCount(100))
}

func combinationCount(n int) int {
	count := 0
	for m := 0; m <= n; m += 5 {
		count += (100 - m + 2) / 2
	}
	return count
}
