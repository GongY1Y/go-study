package main

import "fmt"

// 如何判断还有几盏灯泡还亮着

func main() {
	num := make([]int, 100)
	for i := 0; i < 100; i++ {
		num[i] = i + 1
	}
	count := totalCount(num, 100)
	fmt.Println("最后总共有", count, "盏灯亮着")
}

func factorIsOdd(a int) bool {
	var total int
	for i := 1; i <= a; i++ {
		if a%i == 0 {
			total++
		}
	}
	if total%2 == 1 {
		return true
	} else {
		return false
	}
}

func totalCount(num []int, n int) int {
	var count int
	for i := 0; i < n; i++ {
		if factorIsOdd(num[i]) {
			fmt.Print(num[i], " ")
			count++
		}
	}
	fmt.Println()
	return count
}
