package main

import "fmt"

// 如何找出数组中出现奇数次的数

func main() {
	arr := []int{3, 5, 6, 6, 5, 7, 2, 2}
	getNumXor(arr)
}

func getNumXor(arr []int) {
	if arr == nil || len(arr) == 0 {
		return
	}

	result := 0
	position := 0

	for _, v := range arr {
		result ^= v
	}

	a := result

	for i := result; i&1 == 0; i = i >> 1 {
		position++
	}

	for _, v := range arr {
		if (v>>position)&1 == 1 {
			a ^= v
		}
	}

	fmt.Println(a)
	fmt.Println(result ^ a)
}
