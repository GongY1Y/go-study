package main

import "fmt"

// 如何从三个有序数组中找出它们的公共元素

func main() {
	arr1 := []int{2, 5, 12, 20, 45, 85}
	arr2 := []int{16, 19, 20, 85, 200}
	arr3 := []int{3, 4, 15, 20, 37, 72, 85, 190}
	findCommon(arr1, arr2, arr3)
}

func findCommon(arr1, arr2, arr3 []int) {
	n1, n2, n3 := len(arr1), len(arr2), len(arr3)
	for i, j, k := 0, 0, 0; i < n1 && j < n2 && k < n3; {
		if arr1[i] == arr2[j] && arr2[j] == arr3[k] {
			fmt.Print(arr1[i], " ")
			i++
			j++
			k++
		} else if arr1[i] < arr2[j] {
			i++
		} else if arr2[j] < arr3[k] {
			j++
		} else {
			k++
		}
	}
}
