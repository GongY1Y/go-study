package main

import "fmt"

// 如何寻找最多的覆盖点

func main() {
	arr := []int{1, 3, 7, 8, 10, 11, 12, 13, 15, 16, 17, 19, 25}
	fmt.Println("最长覆盖点数：", maxCover(arr, 8))
}

func maxCover(a []int, l int) int {
	count := 0
	maxCount := 0
	start := 0
	n := len(a)

	for i := 0; i < n; i++ {
		count = 1
		for j := i + 1; j < n && a[j]-a[i] <= l; j++ {
			count++
		}
		if count > maxCount {
			maxCount = count
			start = i
		}
	}

	fmt.Print("覆盖的坐标点：")
	for i := start; i < start+maxCount; i++ {
		fmt.Print(a[i], " ")
	}
	fmt.Println()
	return maxCount
}
