package main

import "fmt"

// 如何对磁盘进行分区

func main() {
	d := []int{120, 120, 120}
	p := []int{60, 60, 80, 20, 80}
	if isAllocatable(d, p) {
		fmt.Println("分配成功")
	} else {
		fmt.Println("分配失败")
	}
}

func isAllocatable(d, p []int) bool {
	dIndex := 0
	for _, v := range p {
		for dIndex < len(d) && v > d[dIndex] {
			dIndex++
		}
		if dIndex >= len(d) {
			return false
		}
		d[dIndex] -= v
	}
	return true
}
