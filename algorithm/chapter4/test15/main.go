package main

import "fmt"

// 如何对数组进行循环移位

func main() {
	arr := []int{1, 2, 3, 4, 5, 6, 7, 8}
	rightShift(arr, 4)
	for _, v := range arr {
		fmt.Print(v, " ")
	}
	fmt.Println()
}

// 1 2 3 4 5
// 5 4 3 2 1
// 3 4 5 1 2

func rightShift(arr []int, k int) {
	n := len(arr)
	k = k % n
	reverse(arr, 0, n-1)
	reverse(arr, 0, k-1)
	reverse(arr, k, n-1)
}

func reverse(arr []int, start, end int) {
	for start < end {
		arr[start], arr[end] = arr[end], arr[start]
		start++
		end--
	}
}
