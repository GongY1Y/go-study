package main

import "fmt"

// 如何按要求构造新的数组

func main() {
	a := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	b := make([]int, len(a))
	calculate(a, b)
	fmt.Println(b)
}

func calculate(a, b []int) {
	b[0] = 1
	length := len(a)
	for i := 1; i < length; i++ {
		b[i] = b[i-1] * a[i-1]
	}
	b[0] = a[length-1]
	for i := length - 2; i >= 1; i-- {
		b[i] *= b[0]
		b[0] *= a[i]
	}
}
