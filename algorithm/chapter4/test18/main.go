package main

import "fmt"

// 如何判断请求能否在给定的存储条件下完成

func main() {
	r := []int{10, 15, 23, 20, 6, 9, 7, 16}
	o := []int{2, 7, 8, 4, 5, 8, 6, 8}
	scheduleResult := schedule(r, o, 50)
	if scheduleResult {
		for i := 0; i < 8; i++ {
			fmt.Print(r[i], ",", o[i], " ")
		}
	}
}

func schedule(r, o []int, m int) bool {
	bubbleSort(r, o)
	left := m
	for i, v := range r {
		if left < v {
			return false
		} else {
			left -= o[i]
		}
	}
	return true
}

func bubbleSort(r, o []int) {
	ll := len(r)
	for i := 0; i < ll-1; i++ {
		for j := 0; j < ll-i-1; j++ {
			if r[j]-o[j] < r[j+1]-o[j+1] {
				r[j], r[j+1] = r[j+1], r[j]
				o[j], o[j+1] = o[j+1], o[j]
			}
		}
	}
}
