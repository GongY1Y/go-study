package main

import "fmt"

// 如何对任务进行调度

func main() {
	t := []int{7, 10}
	proTime := calculateProcessTime(t, 6)
	if proTime == nil {
		fmt.Println("分配失败")
		return
	}
	totalTime := proTime[0]
	for i, v := range proTime {
		fmt.Printf("第%v台服务器有%v个任务，执行总时间为：%v", i+1, v/t[i], v)
		if v > totalTime {
			totalTime = v
		}
		fmt.Println()
	}
	fmt.Println("执行完所有任务所需的时间为", totalTime)
}

func calculateProcessTime(t []int, n int) []int {
	// t 记录每台机器执行任务所耗费的时间，n 指的是任务总数
	if t == nil || n <= 0 {
		return nil
	}

	// 机器数量
	m := len(t)

	// 每台机器的总执行时间
	proTime := make([]int, m)
	for i := 0; i < n; i++ {
		// 最快结束时刻
		minTime := proTime[0] + t[0]
		minIndex := 0
		for j := 1; j < m; j++ {
			if minTime > proTime[j]+t[j] {
				minTime = proTime[j] + t[j]
				minIndex = j
			}
		}
		// 那个执行任务会使执行结束之间更早，就把任务分配给谁
		proTime[minIndex] += t[minIndex]
	}
	return proTime
}
