package main

import "fmt"

// 如何找出数组中丢失的数

func main() {
	arr := []int{1, 4, 3, 2, 7, 5}
	fmt.Println(getNumXor(arr))
}

func getNumXor(arr []int) int {
	if arr == nil || len(arr) == 0 {
		return -1
	}

	a := arr[0]
	b := 1

	length := len(arr)

	for i := 1; i < length; i++ {
		a ^= arr[i]
	}

	for i := 2; i < length+1; i++ {
		b ^= 1
	}

	return a ^ b
}
