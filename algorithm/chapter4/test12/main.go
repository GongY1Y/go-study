package main

import "fmt"

// 如何旋转数组

func main() {
	arr := [][]int{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}
	rotateArr(arr)
}

func rotateArr(arr [][]int) {
	row := 0
	col := 0
	length := len(arr)

	for i := length - 1; i > 0; i-- {
		row = 0
		col = i
		for col < length {
			fmt.Print(arr[row][col], " ")
			row++
			col++
		}
		fmt.Println()
	}

	for i := 0; i < length; i++ {
		row = i
		col = 0
		for row < length {
			fmt.Print(arr[row][col], " ")
			row++
			col++
		}
		fmt.Println()
	}
}
