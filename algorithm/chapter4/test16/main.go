package main

import "fmt"

// 如何在有规模的二维数组中进行高效的数据查找

func main() {
	arr := [][]int{
		{0, 1, 2, 3, 4},
		{10, 11, 12, 13, 14},
		{20, 21, 22, 23, 24},
		{30, 31, 32, 33, 34},
		{40, 41, 42, 43, 44},
	}
	fmt.Println(findWithBinary(arr, 14))
	fmt.Println(findWithBinary(arr, 17))
}

func findWithBinary(arr [][]int, data int) bool {
	if arr == nil {
		return false
	}

	rows := len(arr)
	columns := len(arr[0])

	for i, j := 0, columns-1; i < rows && j >= 0; {
		if arr[i][j] == data {
			return true
		} else if arr[i][j] < data {
			i++
		} else {
			j--
		}
	}

	return false
}
