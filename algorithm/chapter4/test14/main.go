package main

import "fmt"

// 如何求集合的所有子集

func main() {
	arr := []rune{'a', 'b', 'c'}
	mask := []int{0, 0, 0}
	getAllSubset(arr, mask, 0)
	str := "abc"
	result := getAllSubset2(str)
	for _, v := range result {
		fmt.Println(v)
	}

}

// 位图法
func getAllSubset(arr []rune, mask []int, c int) {
	length := len(arr)
	if length == c {
		fmt.Print("{ ")
		for i, v := range arr {
			if mask[i] == 1 {
				fmt.Print(string(v), " ")
			}
		}
		fmt.Println(" }")
	} else {
		mask[c] = 1
		getAllSubset(arr, mask, c+1)
		mask[c] = 0
		getAllSubset(arr, mask, c+1)
	}
}

// 迭代法
func getAllSubset2(str string) (arr []string) {
	arr = make([]string, 0)
	if str == "" {
		return arr
	}
	arr = append(arr, string(str[0]))
	for i := 1; i < len(str); i++ {
		l1 := len(arr)
		for j := 0; j < l1; j++ {
			arr = append(arr, arr[j]+string(str[i]))
		}
		arr = append(arr, str[i:i+1])
	}

	return arr
}
