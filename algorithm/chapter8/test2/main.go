package main

import "fmt"

// 如何进行插入排序

func main() {
	array := []int{5, 4, 9, 8, 7, 6, 0, 1, 3, 2}
	insertSort(array)
	fmt.Println(array)
}

func insertSort(array []int) {
	if array == nil {
		return
	}

	for i := 1; i < len(array); i++ {
		tmp := array[i]
		j := i
		if array[j-1] > tmp {
			for j >= 1 && array[j-1] > tmp {
				array[j] = array[j-1]
				j--
			}
		}
		array[j] = tmp
	}
}
