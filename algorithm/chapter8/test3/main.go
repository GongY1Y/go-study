package main

import "fmt"

// 如何进行冒泡排序

func main() {
	array := []int{5, 4, 9, 8, 7, 6, 0, 1, 3, 2}
	bubbleSort(array)
	fmt.Println(array)
}

func bubbleSort(array []int) {
	l := len(array)
	for i := 0; i < l-1; i++ {
		for j := l - 1; j > i; j-- {
			if array[j] < array[j-1] {
				tmp := array[j]
				array[j] = array[j-1]
				array[j-1] = tmp
			}
		}
	}
}
