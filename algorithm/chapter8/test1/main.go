package main

import "fmt"

// 如何进行选择排序

func main() {
	data := []int{5, 4, 9, 8, 7, 6, 0, 1, 3, 2}
	selectSort(data)
	fmt.Println(data)
}

func selectSort(data []int) {
	l := len(data)
	for i := 0; i < l; i++ {
		tmp := data[i]
		flag := i
		for j := i + 1; j < l; j++ {
			if data[j] < tmp {
				tmp = data[j]
				flag = j
			}
		}

		if flag != i {
			data[flag] = data[i]
			data[i] = tmp
		}
	}
}
