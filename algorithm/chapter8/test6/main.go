package main

import "fmt"

// 如何进行希尔排序

func main() {
	array := []int{5, 4, 9, 8, 7, 6, 0, 1, 3, 2}
	shellSort(array)
	fmt.Println(array)
}

func shellSort(array []int) {
	l := len(array)
	for h := l / 2; h > 0; h = h / 2 {
		for i := h; i < l; i++ {
			temp := array[i]
			var j int
			for j = i - h; j >= 0; j -= h {
				if temp < array[j] {
					array[j+h] = array[j]
				} else {
					break
				}
			}
			array[j+h] = temp
		}
	}
}
