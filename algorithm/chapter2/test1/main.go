package main

import (
	"fmt"
	"go-study/algorithm/common"
)

func main() {
	defer func() {
		if err := recover(); err != nil {
			fmt.Println(err)
		}
	}()

	fmt.Println("链表构建栈结构")
	linkedStack := &common.LinkedStack{
		Head: &common.LNode{},
	}
	linkedStack.Push(1)
	fmt.Println("栈顶元素为：", linkedStack.Top())
	fmt.Println("栈大小为：", linkedStack.Size())
	linkedStack.Pop()
	fmt.Println("弹栈成功：", linkedStack.Size())
	linkedStack.Pop()
}
