package main

import (
	"fmt"
	"go-study/algorithm/common"
)

func main() {
	defer func() {
		if err := recover(); err != nil {
			fmt.Println(err)
		}
	}()

	fmt.Println("链表构建队列结构")
	linkedQueue := &common.LinkedQueue{}
	linkedQueue.Push(1)
	linkedQueue.Push(2)
	fmt.Println("队列头元素为：", linkedQueue.GetFront())
	fmt.Println("队列尾元素为：", linkedQueue.GetBack())
	fmt.Println("队列大小为：", linkedQueue.Size())
}
