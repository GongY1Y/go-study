package main

import "fmt"

// 如何从数组中找出满足 a + b = c + d 的两个数对

func main() {
	arr := []int{3, 4, 7, 10, 20, 9, 8}
	findPairs(arr)
}

type pair struct {
	first  int
	second int
}

func findPairs(arr []int) bool {
	// 键为数对的和，值为数对
	sumPair := map[int]*pair{}
	n := len(arr)

	// 遍历数组中所有可能的数对
	for i := 0; i < n; i++ {
		for j := i + 1; j < n; j++ {
			sum := arr[i] + arr[j]
			if _, ok := sumPair[sum]; !ok {
				// 如果这个数对的和在 map 中没有，则放入 map 中
				sumPair[sum] = &pair{i, j}
			} else {
				// map 中已经存在与 sum 相同的数对了，找出来并打印出来
				p := sumPair[sum]
				fmt.Println(arr[p.first], arr[p.second], arr[i], arr[j])
				return true
			}
		}
	}
	return false
}
