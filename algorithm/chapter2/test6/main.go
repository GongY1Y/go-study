package main

import (
	"fmt"
	"go-study/algorithm/common"
)

// 如何用两个栈模拟队列操作

func main() {
	stack := &stackQueue{
		aStack: &common.LinkedStack{
			Head: &common.LNode{},
		},
		bStack: &common.LinkedStack{
			Head: &common.LNode{},
		},
	}

	stack.push(1)
	stack.push(2)
	stack.push(3)

	fmt.Println("队列首元素为：", stack.pop())

	stack.push(4)
	stack.push(5)

	fmt.Println("队列首元素为：", stack.pop())
}

type stackQueue struct {
	aStack *common.LinkedStack
	bStack *common.LinkedStack
}

func (p *stackQueue) push(data int) {
	p.aStack.Push(data)
}

func (p *stackQueue) pop() int {
	if p.bStack.IsEmpty() {
		for !p.aStack.IsEmpty() {
			p.bStack.Push(p.aStack.Pop())
		}
	}
	return p.bStack.Pop()
}
