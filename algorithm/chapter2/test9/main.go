package main

import "fmt"

// 如何从给定的车票中找出旅游路线

func main() {
	input := map[string]string{"西安": "成都", "北京": "上海", "大连": "西安", "上海": "大连"}
	printResult(input)
}

func printResult(input map[string]string) {
	// 用来存储把 input 的键与值调换后的信息
	reverseInput := map[string]string{}
	for k, v := range input {
		reverseInput[v] = k
	}

	// 找到起点
	start := ""
	for k := range input {
		if _, ok := reverseInput[k]; !ok {
			start = k
			break
		}
	}

	if start == "" {
		fmt.Println("输入不合理")
		return
	}

	to := input[start]
	for to != "" {
		fmt.Println(start, "->", to)
		start = to
		to = input[to]
	}
}
