package main

import (
	"fmt"
	"go-study/algorithm/standard"
)

// 如何复制二叉树

func main() {
	data := []int{-1, 3, 9, 6, -1}
	root := standard.ArrayToTree(data, 0, len(data)-1)
	dupRoot := dupTree(root)
	standard.PrintTreeMidOrder(root)
	fmt.Println()
	standard.PrintTreeMidOrder(dupRoot)
	fmt.Println()
}

func dupTree(root *standard.BNode) *standard.BNode {
	if root == nil {
		return nil
	}
	dupRoot := standard.NewBNode()
	dupRoot.Data = root.Data
	dupRoot.LeftChild = dupTree(root.LeftChild)
	dupRoot.RightChild = dupTree(root.RightChild)
	return dupRoot
}
