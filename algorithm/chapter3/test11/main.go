package main

import (
	"fmt"
	"go-study/algorithm/standard"
)

// 如何对二叉树进行镜像反转

func main() {
	data := []int{1, 2, 3, 4, 5, 6, 7}
	root := standard.ArrayToTree(data, 0, len(data)-1)
	standard.PrintTreeMidOrder(root)
	fmt.Println()
	reverseTree(root)
	standard.PrintTreeMidOrder(root)
	fmt.Println()
}

func reverseTree(root *standard.BNode) {
	if root == nil {
		return
	}

	reverseTree(root.LeftChild)
	reverseTree(root.RightChild)

	root.LeftChild, root.RightChild = root.RightChild, root.LeftChild
}
