package main

import (
	"fmt"
	"go-study/algorithm/standard"
)

// 如何把二叉树转换为双向链表

func main() {
	data := []int{1, 2, 3, 4, 5, 6, 7}
	root := standard.ArrayToTree(data, 0, len(data)-1)
	inOrderBSTree(root)
	for current := pHead; current != nil; current = current.RightChild {
		fmt.Print(current.Data, " ")
	}
	fmt.Println()
	for current := pEnd; current != nil; current = current.LeftChild {
		fmt.Print(current.Data, " ")
	}
	fmt.Println()
}

var pHead *standard.BNode
var pEnd *standard.BNode

func inOrderBSTree(root *standard.BNode) {
	if root == nil {
		return
	}
	inOrderBSTree(root.LeftChild)
	root.LeftChild = pEnd
	if pEnd == nil {
		pHead = root
	} else {
		pEnd.RightChild = root
	}
	pEnd = root
	inOrderBSTree(root.RightChild)
}
