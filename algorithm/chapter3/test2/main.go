package main

import (
	"fmt"
	"go-study/algorithm/standard"
)

// 如何把一个有序整数数组放到二叉树中

func main() {
	data := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	fmt.Println("数组：", data)
	root := arrayToTree(data, 0, len(data)-1)
	fmt.Println("转换成树的中序遍历为：")
	standard.PrintTreeMidOrder(root)
	fmt.Println()
}

func arrayToTree(arr []int, start int, end int) *standard.BNode {
	var root *standard.BNode
	if end >= start {
		root = standard.NewBNode()
		mid := (start + end + 1) / 2
		root.Data = arr[mid]
		root.LeftChild = arrayToTree(arr, start, mid-1)
		root.RightChild = arrayToTree(arr, mid+1, end)
	}

	return root
}
