package main

import (
	"fmt"
	"go-study/algorithm/standard"
)

// 如何从顶部开始逐层打印二叉树节点数据

func main() {
	data := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
	fmt.Println("数组：", data)
	root := standard.ArrayToTree(data, 0, len(data)-1)
	fmt.Print("树的层序遍历结果为：")
	printTreeLayer(root)
	fmt.Println()
}

func printTreeLayer(root *standard.BNode) {
	if root == nil {
		return
	}
	var p *standard.BNode
	queue := standard.NewSliceQueue()
	queue.EnQueue(root)
	for queue.Size() > 0 {
		p = queue.DeQueue().(*standard.BNode)
		fmt.Print(p.Data, " ")
		if p.LeftChild != nil {
			queue.EnQueue(p.LeftChild)
		}
		if p.RightChild != nil {
			queue.EnQueue(p.RightChild)
		}
	}
}
