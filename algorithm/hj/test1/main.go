package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {

	reader := bufio.NewReader(os.Stdin)
	command, _ := reader.ReadString('\n')

	splitArr := strings.Fields(command)

	strLen := len(splitArr)
	if len(splitArr) > 0 {
		fmt.Println(len(splitArr[strLen-1]))
	} else {
		fmt.Println(0)
	}

}
