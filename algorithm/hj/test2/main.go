package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	reader := bufio.NewReader(os.Stdin)
	s1, _ := reader.ReadString('\n')
	s2, _ := reader.ReadString('\n')

	s1 = strings.ToLower(s1)
	s2 = strings.ToLower(s2)

	count := 0
	for _, c := range s1 {
		if c == int32(s2[0]) {
			count++
		}
	}
	fmt.Print(count)
}