package main

import "fmt"

type Person struct {
	name string
	age int
}

func main() {
	p := new(Person)
	fmt.Println(p.age)
}
