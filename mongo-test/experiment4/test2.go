package main

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func main() {
	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		panic(err.Error())
	}

	defer func() {
		if err = client.Disconnect(context.TODO()); err != nil {
			panic(err)
		}
	}()

	course := client.Database("user201800301137").Collection("course")
	filter := bson.M{
		"score": 5,
	}
	update := bson.M{
		"$set": bson.M{
			"score": 4,
		},
	}

	result, err := course.UpdateMany(context.TODO(), filter, update)
	if err != nil {
		panic(err.Error())
	}

	fmt.Printf("Documents updated %v\n", result.ModifiedCount)

}
