package main

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func main() {
	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		panic(err.Error())
	}

	defer func() {
		if err = client.Disconnect(context.TODO()); err != nil {
			panic(err.Error())
		}
	}()

	student := client.Database("user201800301137").Collection("student")
	filter := bson.M{
		"name": "张三",
	}
	update := bson.M{
		"$set": bson.M{
			"sid": "200800020105",
			"dname": "SC",
		},
	}

	result, err := student.UpdateOne(context.TODO(), filter, update)
	if err != nil {
		panic(err.Error())
	}

	fmt.Printf("Document update with ID : %v\n", result.UpsertedID)
}
