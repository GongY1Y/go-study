package main

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func main() {
	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		panic(err.Error())
	}

	defer func() {
		if err = client.Disconnect(context.TODO()); err != nil {
			panic(err)
		}
	}()

	student := client.Database("user201800301137").Collection("student")
	filter := bson.M{
		"age": bson.M{
			"$lt": 20,
		},
	}

	cursor, err := student.Find(context.TODO(), filter)
	if err != nil {
		panic(err)
	}

	var results, results2 []bson.M
	if err = cursor.All(context.TODO(), &results); err != nil {
		panic(err)
	}
	for _, result := range results {
		results2 = append(results2, bson.M{
			"name": result["name"],
			"sex":  result["sex"],
		})
	}
	fmt.Println(results2)
}
