package main

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func main() {
	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		panic(err.Error())
	}

	defer func() {
		if err = client.Disconnect(context.TODO()); err != nil {
			panic(err.Error())
		}
	}()

	student := client.Database("user201800301137").Collection("student")
	doc := bson.M{
		"sid":      "200800020104",
		"name":     "张三",
		"sex":      "男",
		"age":      22,
		"birthday": "1997-5-5",
		"dname":    "CS",
		"class":    "2008",
	}

	result, err := student.InsertOne(context.TODO(), doc)
	if err != nil {
		panic(err.Error())
	}

	fmt.Println("Document insert with ID:", result.InsertedID)
}
