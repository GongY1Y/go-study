package main

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func main() {
	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		panic(err.Error())
	}

	defer func() {
		if err = client.Disconnect(context.TODO()); err != nil {
			panic(err)
		}
	}()

	course := client.Database("user201800301137").Collection("course")
	docs := []interface{}{
		bson.M{
			"cid":   "100001",
			"name":  "Math",
			"fcid":  "",
			"score": 5,
		},
		bson.M{
			"cid":   "100002",
			"name":  "cpp",
			"fcid":  "100001",
			"score": 2,
		},
	}

	result, err := course.InsertMany(context.TODO(), docs)
	if err != nil {
		panic(err.Error())
	}

	fmt.Printf("%d documents inserted with IDs:\n", len(result.InsertedIDs))
	for _, id := range result.InsertedIDs {
		fmt.Printf("\t%s\n", id)
	}

}
