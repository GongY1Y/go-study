package main

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func main() {
	clientOptions := options.Client().ApplyURI("mongodb://localhost:27017")
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		panic(err.Error())
	}

	defer func() {
		if err = client.Disconnect(context.TODO()); err != nil {
			panic(err)
		}
	}()

	studentCourse := client.Database("user201800301137").Collection("student_course")

	filter := [...]interface{}{
		bson.M{
			"$group": bson.M{
				"_id": "$cid",
				"count": bson.M{
					"$sum": 1,
				},
			},
		},
		bson.M{
			"$sort": bson.M{
				"count": -1,
			},
		},
		bson.M{
			"$limit": 10,
		},
	}

	cursor, err := studentCourse.Aggregate(context.TODO(), filter)
	if err != nil {
		panic(err.Error())
	}

	var results []bson.M
	if err = cursor.All(context.TODO(), &results); err != nil {
		panic(err.Error())
	}

	for _, result := range results {
		fmt.Println(result)
	}
}
