package main

import (
	"fmt"
	"log"
	"net/http"

	_ "net/http/pprof"

	"chatroom/global"
	"chatroom/server"
)

var addr = ":2022"

func init() {
	global.Init()
}

func main() {
	fmt.Printf(addr)

	server.RegisterHandle()

	log.Fatal(http.ListenAndServe(addr, nil))
}
