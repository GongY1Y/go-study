module chatroom

go 1.16

require (
	github.com/fsnotify/fsnotify v1.5.1
	github.com/klauspost/compress v1.13.6 // indirect
	github.com/spf13/cast v1.4.1
	github.com/spf13/viper v1.9.0
	golang.org/x/sys v0.0.0-20211107104306-e0b2ad06fe42 // indirect
	golang.org/x/text v0.3.7 // indirect
	nhooyr.io/websocket v1.8.7
)
