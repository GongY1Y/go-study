package errcode

type Code int

const (
	WebSocketSuccessMessage = 50001
	WebSocketSuccess        = 50002
	WebSocketEnd            = 50003
	WebSocketOnlineReply    = 50004
	WebSocketOfflineReply   = 50005
	WebSocketLimit          = 50006
)

func Msg(c Code) string {
	return codeMsg[c]
}
