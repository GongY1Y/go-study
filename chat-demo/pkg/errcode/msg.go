package errcode

var codeMsg = map[Code]string{
	WebSocketSuccessMessage: "解析content内容消息",
	WebSocketSuccess:        "发送消息，请求历史记录操作成功",
	WebSocketEnd:            "请求历史记录，但没有更多记录了",
	WebSocketOnlineReply:    "针对回复信息在线应答成功",
	WebSocketOfflineReply:   "针对回复消息离线回答成功",
	WebSocketLimit:          "请求收到限制",
}
