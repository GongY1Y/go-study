package main

import (
	"chat/config"
	"chat/router"
	"chat/service"
)

func main() {
	config.Init()
	go service.Manager.Start()
	r := router.NewRouter()
	_ = r.Run(config.HttpPort)
}
