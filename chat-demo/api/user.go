package api

import (
	"chat/service"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

func UserRegister(c *gin.Context) {
	var userRegisterService service.UserRegisterService
	if err := c.ShouldBind(&userRegisterService); err != nil {
		c.JSON(400, ErrorResponse(err))
		logrus.Info(err)
	} else {
		res := userRegisterService.Register()
		c.JSON(200, res)
	}
}
