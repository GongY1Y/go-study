package cache

import (
	"fmt"
	"github.com/go-redis/redis"
	"github.com/sirupsen/logrus"
	"gopkg.in/ini.v1"
	"strconv"
)

var (
	RedisClient *redis.Client

	RedisDb     string
	RedisAddr   string
	RedisPw     string
	RedisDbName string
)

func init() {
	// 加载配置信息文件
	file, err := ini.Load("./config/config.ini")
	if err != nil {
		fmt.Println("redis ini load failed", err)
	}
	// 读取配置信息文件内容
	loadRedis(file)
	// redis 连接
	Redis()
}

func loadRedis(file *ini.File) {
	RedisDb = file.Section("redis").Key("RedisDb").String()
	RedisAddr = file.Section("redis").Key("RedisAddr").String()
	RedisPw = file.Section("redis").Key("RedisPw").String()
	RedisDbName = file.Section("redis").Key("RedisDbName").String()
}

func Redis() {
	db, _ := strconv.Atoi(RedisDbName)
	client := redis.NewClient(&redis.Options{
		Addr: RedisAddr,
		DB:   db,
	})
	_, err := client.Ping().Result()
	if err != nil {
		logrus.Info(err)
		panic(err)
	}
	RedisClient = client
}
