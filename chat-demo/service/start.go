package service

import (
	"chat/pkg/errcode"
	"encoding/json"
	"fmt"
	"github.com/gorilla/websocket"
)

func (manager *ClientManager) Start() {
	for {
		fmt.Println("----监听管道通信----")
		select {
		case conn := <-Manager.Register:
			fmt.Println("有新连接：", conn.ID)
			Manager.Clients[conn.ID] = conn // 把连接添加进用户管理
			replyMsg := ReplyMsg{
				Code:    errcode.WebSocketSuccess,
				Content: "已经连接到服务器",
			}
			msg, _ := json.Marshal(replyMsg)
			_ = conn.Socket.WriteMessage(websocket.TextMessage, msg)
		}
	}
}
