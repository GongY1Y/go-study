package main

import (
	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	//r.Handle("GET", "/", func(c *gin.Context) {
	//	c.JSON(200, gin.H{"message": "Hello World!"})
	//})
	r.GET("/", func(c *gin.Context) {
		c.JSON(200, gin.H{"message": "Hello World!"})
	})
	err := r.Run()
	//err := http.ListenAndServe(":8080", r)
	if err != nil {
		return
	}
}
